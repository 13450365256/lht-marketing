package com.lht.marketing.service;

import com.lht.marketing.model.MarketingExpenses;
import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.entitybase.response.CommonQueryPageResponse;

import java.util.List;

/**
 * 营销交易流水服务接口
 */
public interface MarketingExpensesService {

    /**
     * 分页查询营销交易流水
     * @param commonQueryPageRequest
     * @return
     */
    CommonQueryPageResponse<List<MarketingExpenses>> queryMarketingExpenses(CommonQueryPageRequest<MarketingExpenses> commonQueryPageRequest);

}
