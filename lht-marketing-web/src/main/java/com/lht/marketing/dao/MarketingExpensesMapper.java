package com.lht.marketing.dao;

import com.lht.marketing.model.MarketingExpenses;
import tk.mybatis.mapper.common.Mapper;

public interface MarketingExpensesMapper extends Mapper<MarketingExpenses> {

}