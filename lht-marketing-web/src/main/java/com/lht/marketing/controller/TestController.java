package com.lht.marketing.controller;

import com.lht.marketing.handle.remote.QuerySmsTemplateDetailRemoteService;
import com.luckwine.parent.entitybase.request.CommonRequest;
import com.luckwine.synthesize.model.SmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
public class TestController {

    @Resource
    private QuerySmsTemplateDetailRemoteService querySmsTemplateDetailRemoteService;

    @RequestMapping(value = "/remote",method = RequestMethod.GET)
    public void testRemote(HttpServletResponse response) {

        CommonRequest<SmsTemplate> commonRequest = new CommonRequest<>();
        SmsTemplate smsTemplate = new SmsTemplate();
        smsTemplate.setSmsId("1");
        commonRequest.setRequest(smsTemplate);

        System.out.println(querySmsTemplateDetailRemoteService.call(commonRequest).toString()) ;

    }

}
