package com.lht.marketing.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.lht.marketing.handle.local.QueryMarketingExpensesService;
import com.lht.marketing.model.MarketingExpenses;
import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.entitybase.response.CommonQueryPageResponse;

import javax.annotation.Resource;
import java.util.List;

/**
 * validation = "true"，开启服务端校验
 */
@Service(validation = "true")
public class MarketingExpensesServiceImpl implements MarketingExpensesService{

    @Resource
    private QueryMarketingExpensesService queryMarketingExpensesService;

    @Override
    public CommonQueryPageResponse<List<MarketingExpenses>> queryMarketingExpenses(CommonQueryPageRequest<MarketingExpenses> commonQueryPageRequest) {
        return queryMarketingExpensesService.call(commonQueryPageRequest);
    }

}
