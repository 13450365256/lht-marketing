package com.lht.marketing.handle.remote;

import com.alibaba.dubbo.config.annotation.Reference;
import com.luckwine.parent.entitybase.request.CommonRequest;
import com.luckwine.parent.entitybase.response.CommonResponse;
import com.luckwine.parent.template.SingleRemoteTemplate;
import com.luckwine.synthesize.model.SmsTemplate;
import com.luckwine.synthesize.service.SmsTemplateService;
import org.springframework.stereotype.Service;

@Service
public class QuerySmsTemplateDetailRemoteService extends SingleRemoteTemplate<SmsTemplate,SmsTemplate> {

    /**
     * validation = "true":开启客户端校验（默认关闭）
     */
    @Reference
//    @Reference(validation = "true")
    private SmsTemplateService smsTemplateService;

    @Override
    protected CommonResponse<SmsTemplate> callRemote(CommonRequest<SmsTemplate> request) {
        return smsTemplateService.smsTemplateDetail(request);
    }
}
