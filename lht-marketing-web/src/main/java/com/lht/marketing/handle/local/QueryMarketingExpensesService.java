package com.lht.marketing.handle.local;

import com.lht.marketing.dao.MarketingExpensesMapper;
import com.lht.marketing.model.MarketingExpenses;
import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.template.QueryPageTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  分页查询营销流水
 */
@Service
public class QueryMarketingExpensesService extends QueryPageTemplate<MarketingExpenses,List<MarketingExpenses>, MarketingExpenses> {

    @Resource
    private MarketingExpensesMapper marketingExpensesMapper;

    @Override
    protected List<MarketingExpenses> callInner(CommonQueryPageRequest<MarketingExpenses> request) throws Exception {
        return marketingExpensesMapper.selectAll();
    }

    @Override
    protected List<MarketingExpenses> transformationResponse(List<MarketingExpenses> response, CommonQueryPageRequest<MarketingExpenses> request) throws Exception {
        return response;
    }
}
